﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour
{

    public Data_Client client;

    public static Main sharedInstance;

    public string IP_Adress;

    void Awake()
    {
        sharedInstance = this;
    }

    void Start()
    {
        client = new Data_Client(IP_Adress);
        byte[] data = new byte[] { 0, 0, 0, 0, 133, 78, 0, 0 };
        client.Send(data);
    }
    void OnDisable()
    {
        client.Stop();
    }
}
