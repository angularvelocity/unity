﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.Threading;
using System;
using ProtoBuf;
using System.IO;

public class Data_Client : MonoBehaviour
{

    private TcpClient _tcpClient;


    public Data_Client(string ipAddress)
    {
        _tcpClient = new TcpClient();
        _tcpClient.Connect(ipAddress, 9000);
        _tcpClient.ReceiveTimeout = 1000;

        startListening();
    }

    private Thread t;

    private void startListening()
    {
        t = new Thread(listen);
        t.Start();
    }

    public void Stop()
    {
        Debug.Log("QUITING...");
        t.Abort();
    }

    public float Ball_Z;
    public float Ball_X;

    private void listen()
    {
        byte[] buffer = new byte[4];
        while (_tcpClient.Client.Receive(buffer, 4, SocketFlags.None) > 0)
        {
            while (BitConverter.ToInt32(buffer, 0) != 300300)
                _tcpClient.Client.Receive(buffer, 4, SocketFlags.None);

            try
            {
                buffer = new byte[8];
                _tcpClient.Client.Receive(buffer, 8, SocketFlags.None);
                int length = BitConverter.ToInt32(buffer, 0);
                int type = BitConverter.ToInt32(buffer, 4);
                int received = 0;
                buffer = new byte[length];
                while (received < length)
                {
                    received += _tcpClient.Client.Receive(buffer, received, length - received, SocketFlags.None);
                }
                switch (type)
                {
                    case (int)Global.DataType.SC_Ball:
                        Global.BallPosition bp;
                        using (var ms = new MemoryStream(buffer))
                            bp = Serializer.Deserialize<Global.BallPosition>(ms);
                        Ball_Z = (float)bp.Distance;
                        Ball_X = (float)bp.Position;
                        Debug.Log("Ball Lane: " + bp.Lane + " " + "Ball Distance: " + bp.Distance + " " + "Ball Position: " + bp.Position);
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Debug.Log("ERROR: " + ex.Message);
            }
            buffer = new byte[4];
        }
        Debug.Log("Exit listen()");
    }

    public void Send(byte[] data)
    {
        _tcpClient.Client.Send(data);
    }
}
