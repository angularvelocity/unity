﻿using UnityEngine;
using ProtoBuf;
using System.Collections;
using System;

public class Global : MonoBehaviour {

	public enum DataType
	{
		/*
            10000 - Server -> Client
            20000 - Client -> Server

            100 - bowling
        */
		SC_FullScan = 10100,
		SC_Ball = 10101,
		SC_LaneInfo = 10103,
		SC_CalibrationSet = 10104,
		SC_ShotStarted = 10111,
		SC_ShotNotCompleted = 10112,
		SC_ShotCompleted = 10113,

		CS_GetFullScan = 20100,
		CS_StartStream = 20101,
		CS_StopStream = 20102,
		CS_GetLaneInfo = 20103,
		CS_SetCalibration = 20104,
	}

	[ProtoContract]
	public class BallPosition
	{
		[ProtoMember(1)]
		public int Lane { get; set; }
		[ProtoMember(2)]
		public double Position { get; set; }
		[ProtoMember(3)]
		public double Distance { get; set; }
		[ProtoMember(4)]
		public TimeSpan Time { get; set; }
		[ProtoMember(5)]
		public int Accuracy { get; set; }

		public BallPosition() { }

	}

}
